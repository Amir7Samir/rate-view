//
//  ViewController.swift
//  RateView
//
//  Created by Amir on 5/9/19.
//  Copyright © 2019 Amir. All rights reserved.
//

import UIKit
import Cosmos
import TinyConstraints

class ViewController: UIViewController {

    @IBOutlet weak var rateView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        


    }
    
    func doneRated() {
        rateView.settings.updateOnTouch = false
        rateView.text = ""
    }
    
    func rateAgain(){
        rateView.settings.updateOnTouch = true
        rateView.text = "Rate Me "
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
      doneRated()
    }
    
    @IBAction func rateAgainTapped(_ sender: UIButton) {
        rateAgain()
    }
    
}

